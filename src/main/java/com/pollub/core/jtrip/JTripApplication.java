package com.pollub.core.jtrip;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JTripApplication {

    public static void main(String[] args) {
        SpringApplication.run(JTripApplication.class, args);
    }

}
